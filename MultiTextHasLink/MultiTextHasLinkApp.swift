//
//  MultiTextHasLinkApp.swift
//  MultiTextHasLink
//
//  Created by Sarathi Kannan S on 03/07/22.
//

import SwiftUI

@main
struct MultiTextHasLinkApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
