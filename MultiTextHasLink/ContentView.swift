//
//  ContentView.swift
//  MultiTextHasLink
//
//  Created by Sarathi Kannan S on 03/07/22.
//

import SwiftUI
import UIKit

struct ContentView: View {
    @State var isLinkSelected = false
    var body: some View {
        NavigationView{
            VStack{
                NavigationLink(destination: MultiTextView(), isActive: $isLinkSelected){
                    VStack{
                        
                    }
                }
                    Text("HyperLink Text Label Demo")
                        .font(.title3)
                        .padding([.trailing, .leading])
                    Spacer()
                    TextLabelWithHyperLink(isLinkSelected: $isLinkSelected)
                }
                
            }
            .padding()
        
        
        
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct TextLabelWithHyperLink: UIViewRepresentable {
    
    var callBack: (() -> Void)? = nil
    
    @Binding var isLinkSelected: Bool
    init(isLinkSelected: Binding<Bool>){
        self._isLinkSelected = isLinkSelected
    }
     
      func makeCoordinator() -> Coordinator {
                  Coordinator(self)
    }
    
    func makeUIView(context: Context) -> some UITextView {
        let standardTextAttribute: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 20),
            NSAttributedString.Key.foregroundColor : UIColor.gray
        ]
        let attributedText = NSMutableAttributedString(string: "You can go to ")
        attributedText.addAttributes(standardTextAttribute, range: NSMakeRange(0, attributedText.length))
        
        let hyperLinkedAttributes : [NSAttributedString.Key: Any] = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.blue,
                                                                     NSAttributedString.Key.link: "stackoverflow"
        ]
        let textWithHyperLinkText = NSMutableAttributedString(string: "stack overflow site")
        textWithHyperLinkText.addAttributes(hyperLinkedAttributes, range: NSMakeRange(0, textWithHyperLinkText.length))
        attributedText.append(textWithHyperLinkText)
        let endOfAttrString = NSMutableAttributedString(string: " end enjoy it using old-school UITextView and UIViewRepresentable")
        endOfAttrString.addAttributes(standardTextAttribute,  range: NSMakeRange(0, endOfAttrString.length))
        attributedText.append(endOfAttrString)
        
        let textView = UITextView()
        textView.attributedText = attributedText
        textView.isEditable = false
        textView.textAlignment = .center
        textView.isSelectable = true
        textView.delegate = context.coordinator
        return textView
    }
    
    func updateUIView(_ uiView: UIViewType, context: Context) {
        
    }
    
    
    
class Coordinator: NSObject, UITextViewDelegate{
            
            var parent:TextLabelWithHyperLink
    
            init(_ parent:TextLabelWithHyperLink) {
                    self.parent = parent
                }
            
            @available(iOS, deprecated: 10.0)
            func textView(_ textView: UITextView, shouldInteractWith url: URL, in characterRange: NSRange) -> Bool {
                    //...
                    return false
                }
    
            //For iOS 10
            @available(iOS 10.0, *)
            func textView(_ textView: UITextView, shouldInteractWith url: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        
        print("Url string \(url.absoluteString)")
        if(url.absoluteString == "stackoverflow"){
            self.parent.isLinkSelected = true
//            return true
        }
        return true
    }
}
     
}




