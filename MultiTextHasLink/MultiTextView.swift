//
//  MultiTextView.swift
//  MultiTextHasLink
//
//  Created by Sarathi Kannan S on 03/07/22.
//

import SwiftUI

struct MultiTextView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct MultiTextView_Previews: PreviewProvider {
    static var previews: some View {
        MultiTextView()
    }
}
